#include "ft_ping.h"

bool    set_so_interface(int sockfd, t_opt *opt)
{
	if (bind(sockfd, (struct sockaddr *)&opt->ifr.ifr_addr, sizeof(opt->ifr.ifr_addr)))
		return (true);
    return (false);
}

bool	set_so_quality(int sockfd, t_opt *opt)
{
	if (setsockopt(sockfd, IPPROTO_IP, IP_TOS,
	&opt->quality, sizeof(opt->quality)))
		return (true);
	return (false);
}

bool	set_so_timestamp(int sockfd, t_opt *opt)
{
	u_char rspace[40];
	__u32 *tmp_rspace;

	rspace[0] = IPOPT_TIMESTAMP;
	rspace[1] = 40;
	rspace[2] = 5;
	rspace[3] = (opt->info & TSONLY) ? IPOPT_TS_TSONLY : IPOPT_TS_TSANDADDR;
	if ((opt->info & TSPRESPEC))
	{
		rspace[3] = IPOPT_TS_PRESPEC;
		int i = 0;
		for (t_prespec *node = opt->prespec; node; node = node->next) {
			tmp_rspace = (__u32*)&rspace[4+i*8];
			*tmp_rspace = ((struct sockaddr_in *)(node->host->ai_addr))->sin_addr.s_addr;
			++i;
		}
	}
	if (setsockopt(sockfd, IPPROTO_IP, IP_OPTIONS, rspace, rspace[1]))
		return (true);
	return (false);
}

bool	set_so_ttl(int sockfd, t_opt *opt)
{
	if (setsockopt(sockfd, IPPROTO_IP, IP_TTL,
    &opt->ttl, sizeof(opt->ttl)) || setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_TTL, &opt->ttl, sizeof(opt->ttl)))
		return (true);
	return (false);
}
