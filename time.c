#include "ft_ping.h"

bool		time_value(struct timeval *st, const char *to_eval,
			size_t lst_hex)
{
	int		c;
	size_t	f;

	if (to_eval[++lst_hex] && (to_eval[lst_hex] != '.'
    || (to_eval[lst_hex + 1] && (to_eval[lst_hex + 1] < '0'
    || to_eval[lst_hex + 1] > '9'))))
        return (false);
    else if (!to_eval[lst_hex])
	{
		st->tv_usec = 0;
        return (true);
	}
    c = 0;
    f = ++lst_hex;
    while (c < 6 && to_eval[lst_hex] <= '9'
    && to_eval[lst_hex] >= '0')
    {
        lst_hex++;
        c++;
    }
    st->tv_usec = ft_atoi(to_eval + f, --c);
    while (++c < 6)
        st->tv_usec *= 10;
    return (true);
}

float		time_calc(t_print *print, struct timeval *tv)
{
	struct timeval	now;
	float			ret;

	gettimeofday(&now, NULL);
	ret = (now.tv_sec - tv->tv_sec) * 1000
		+ (float)(now.tv_usec- tv->tv_usec) / 1000;
	if (ret < print->min)
		print->min = ret;
	else if (ret > print->max)
		print->max = ret;
	print->sum +=  ret;
	return (ret);
}

static void dlhandler()
{
	glob_flag = true;
	write(1, "\n", 1);
}

void		set_dltimer(t_opt *opt)
{
	struct itimerspec	dlits;
	struct sigevent		dlsev;
	struct sigaction	sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = dlhandler;
	sigaction(SIGALRM, &sa, NULL);
	dlits.it_value.tv_sec = opt->deadline.tv_sec;
	dlits.it_value.tv_nsec = opt->deadline.tv_usec * 1000;
	dlsev.sigev_notify = SIGEV_SIGNAL;
	dlsev.sigev_signo = SIGALRM;
	timer_create(CLOCK_REALTIME, &dlsev, &(opt->ptimer[0]));
	timer_settime(opt->ptimer[0], 0, &dlits, NULL);
}

static void tampon(int sig __attribute__((unused)),
			siginfo_t *si, void *uc __attribute__((unused)))
{
	t_send	*send;

	send = (t_send *)((unsigned char *)si->si_value.sival_ptr);
	pinger(send);
}

void		set_pingtimer(t_opt *opt, t_send *send)
{
	struct itimerspec		pits;
	struct sigevent			psev;
	struct sigaction		sa;
	static unsigned char	*pack;

	if (glob_flag)
		return ;
	pack = (unsigned char *)send;
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = tampon;
	sigaction(SIGUSR1, &sa, NULL);
	pits.it_value.tv_sec = opt->interval.tv_sec;
	pits.it_value.tv_nsec = opt->interval.tv_usec * 1000;
	pits.it_interval.tv_sec = opt->interval.tv_sec;
	pits.it_interval.tv_nsec = opt->interval.tv_usec * 1000;
	psev.sigev_notify = SIGEV_SIGNAL;
	psev.sigev_signo = SIGUSR1;
	psev.sigev_value.sival_ptr = (void*)pack;
	timer_create(CLOCK_REALTIME, &psev, &(opt->ptimer[1]));
	timer_settime(opt->ptimer[1], 0, &pits, NULL);
}
