#include "ft_ping.h"

bool	ft_strcmp(const char *s1, const char *s2)
{
	int i1;
	int	i2;

	if (!s1)
		return (false);
	i1 = 0;
	i2 = 0;
	while (s1[i1] && s2[i2] && s1[i1] == s2[i2])
	{
		++i1;
		++i2;
	}
	return (!(s1[i1] || s2[i2]));
}

bool    f_is_digit(const char *to_eval, size_t *lst_dig, bool time)
{
    if (!to_eval)
        return (false);
    while (to_eval[*lst_dig] == ' ')
        ++(*lst_dig);
    if (to_eval[*lst_dig] == '+')
        ++(*lst_dig);
    while (to_eval[*lst_dig] >= '0' && to_eval[*lst_dig] <= '9')
        ++(*lst_dig);
    return ((*lst_dig && to_eval[--(*lst_dig)] <= '9'
    && to_eval[*lst_dig] >= '0') || (time && to_eval[*lst_dig + 1] == '.'));
}

int     ft_atoi(const char *s, size_t lst_dig)
{
    long    ret;
    int     mul;

    if (!s)
        return (0);
    ret = 0;
    mul = 1;
    while (lst_dig >= 0 && s[lst_dig] <= '9' && s[lst_dig] >= '0')
    {
        ret += (s[lst_dig--] - '0') * mul;
        mul *= 10;
    }
    return (ret);
}
