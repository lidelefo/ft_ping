#include "ft_ping.h"

static void		parse_opt(t_opt *opt, char **av)
{
	static bool	(*opt_parser[12])(t_opt *, char **) =
		{&set_verbose, &set_help, &set_flood,
		&set_preload, &set_interface, &set_numeric,
		&set_deadline, &set_timeout, &set_quality,
		&set_ttl, &set_timestamp, &set_interval};
	int			i;

	i = 0;
	while (!(*opt_parser[i])(opt, av) && !(opt->info & EOO))
	{
		++i;
		if (i > 11)
		{
			dprintf(2, "ft_ping: invalid option -- '%c'\n",
			av[opt->index[0]][opt->index[1]]);
			display_help();
			clean_exit(opt, EX_USAGE);
		}
	}
}

static void		init_struct(t_opt *opt)
{
	opt->index[0] = 1;
	opt->index[1] = 0;
	opt->info = 0;
	opt->interval.tv_usec = 0;
	opt->interval.tv_sec = 1;
	opt->preload = 0;
	opt->ttl = 64;
	opt->quality = -1;
	opt->destination = NULL;
	opt->prespec = NULL;
	opt->so_opt = NULL;
	opt->ptimer[0] = NULL;
	opt->ptimer[1] = NULL;
}

struct addrinfo	*get_destination(char *dest)
{
	struct addrinfo hints;
	struct addrinfo *tofill;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_RAW;
	hints.ai_flags = AI_V4MAPPED|AI_ADDRCONFIG|AI_CANONNAME;
	hints.ai_protocol = IPPROTO_ICMP;
	if (getaddrinfo(dest, NULL, &hints, &tofill))
	{
		write(2, "ft_ping failed to get destination address\n", 42);
		return (NULL);
	}
	return (tofill);
}

void    clean_exit(t_opt *opt, int exit_value)
{
    void    *to_free;

    while (opt->prespec)
    {
        to_free = opt->prespec;
        opt->prespec = opt->prespec->next;
        free(to_free);
    }
    while (opt->so_opt)
    {
        to_free = opt->so_opt;
        opt->so_opt = opt->so_opt->next;
        free(to_free);
    }
    if (opt->ptimer[0])
        timer_delete((opt->ptimer)[0]);
    if (opt->ptimer[1])
        timer_delete((opt->ptimer)[1]);
    freeaddrinfo(opt->destination);
    close(opt->sockfd);
	exit(exit_value);
}

void    display_help(void)
{
    write(1, "Usage: ft_ping [-fhnv] [-i interval] [-I interface]\n"
    "[-l preload] [-Q tos] [-t ttl] [-T timestamp_option]\n"
    "[-w deadline] [-W timeout] destination\n", 144);
}

int				main(int ac, char **av)
{
	t_opt		opt;

	if (ac < 2)
	{
		display_help();
		return (2);
	}
	init_struct(&opt);
	opt.ac = ac;
	while (opt.index[0] < ac && !(opt.info & EOO))
		parse_opt(&opt, av);
	if (opt.index[0] + 1 != ac)
	{
		display_help();
		clean_exit(&opt, EX_USAGE);
	}
	if (!(opt.destination = get_destination(av[opt.index[0]])))
		clean_exit(&opt, EX_NOHOST);
	ft_ping_exec(&opt);
	return (0);
}
