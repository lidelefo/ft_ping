NAME				=	ft_ping

SRCS				=	exec.c exec_loop.c ft_ping.c flib.c\
						print.c setopt.c setipopt.c\
						setipadd.c setstval.c setsockopt.c\
						time.c
	
OBJS				=	$(SRCS:.c=.o)

CC				=	clang

CFLAGS			=	-Wall -Wextra -Werror

LDLIBS			=	-lcap -lrt

all				:	$(NAME)
					sudo setcap cap_net_admin,cap_net_raw=p $(NAME)

$(NAME)				:	$(OBJS)
					$(CC) $(LDLIBS) $(OBJS) -o $(NAME)

clean				:
					rm -f $(OBJS)

fclean				:	clean
					rm -f $(NAME)

re				:	fclean all
