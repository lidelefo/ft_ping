#include "ft_ping.h"

void				set_node(t_opt *opt, Func func)
{
	t_so_opt	*node;
	t_so_opt	*next;

	if (!(node = (t_so_opt *)malloc(sizeof(t_so_opt))))
	{
		write(2, "ft_ping: failed to allocate memory"
				"on the heap.\n", 48);
		clean_exit(opt, EX_OSERR);
	}
	else if (!opt->so_opt)
	{
		opt->so_opt = node;
		node->next = NULL;
	}
	else
	{
		next = opt->so_opt;
		node->next = next;
		opt->so_opt = node;
	}
	node->func = func;
}

static bool	interface_match(t_opt *opt, char *s)
{
	struct ifaddrs	*tofree;
	struct ifaddrs	*iter;

	getifaddrs(&tofree);
	iter = tofree;
	while (iter)
	{
		if (iter->ifa_addr->sa_family != AF_INET)
		{
			iter = iter->ifa_next;
			continue ;
		}
		inet_ntop(AF_INET, &((struct sockaddr_in *)iter->ifa_addr)->sin_addr, opt->itf_bin, sizeof(struct sockaddr_in));
		if (ft_strcmp(s, opt->itf_bin) || ft_strcmp(s, iter->ifa_name))
		{
			if (!(opt->info & TSPRESPEC))
			{
				opt->ifr.ifr_addr = *iter->ifa_addr;
				snprintf(opt->ifr.ifr_name, sizeof(opt->ifr.ifr_name), "%s", iter->ifa_name);
			}
			freeifaddrs(tofree);
			return (true);
		}
		iter = iter->ifa_next;
	}
	freeifaddrs(tofree);
	return (false);
}

bool		set_interface(t_opt *opt, char **av)
{
	if (ft_strcmp(av[opt->index[0]], "-I") && interface_match(opt, av[opt->index[0] + 1]))
	{
		opt->info |= INTERFACE;
		opt->index[0] += 2;
		opt->index[1] = 0;
		set_node(opt, &set_so_interface);
		return (true);
	}
	return (false);
}

static char	get_prespec(t_opt *opt, char **av)
{
	struct addrinfo *host;
	int				count;
	t_prespec		*next;
	t_prespec		*node;

	count = opt->index[0] + 2;
	while (count != opt->ac - 1 && av[count][0] != '-')
	{
		if (!(host = get_destination(av[count++])))
			clean_exit(opt, EX_NOHOST);
		else if (interface_match(opt, host->ai_canonname))
		{
			if (!(node = (t_prespec *)malloc(sizeof(t_prespec))))
			{
				write(2, "ft_ping: failed to allocate memory on the heap.\n", 48);
				clean_exit(opt, EX_OSERR);
			}
			if (!opt->prespec)
			{
				node->next = NULL;
				opt->prespec = node;
				opt->hops = 0;
			}
			else
			{
				next = opt->prespec;
				node->next = next;
				opt->prespec = node;
			}
			node->host = host;
			++(opt->hops);
		}
	}
	opt->index[0] = count - 2;
	return (opt->hops);
}

bool		set_timestamp(t_opt *opt, char **av)
{
	t_so_opt    *node;
	t_so_opt    *next;
	if (ft_strcmp(av[opt->index[0]], "-T"))
	{
		if (ft_strcmp(av[opt->index[0] + 1], "tsonly"))
			opt->info |= TSONLY;
		else if (ft_strcmp(av[opt->index[0] + 1], "tsandaddr"))
			opt->info |= TSANDADDR;
		else if (ft_strcmp(av[opt->index[0] + 1], "tsprespec")
				&& get_prespec(opt, av) <= 4)
			opt->info |= TSPRESPEC;
		else
		{
			write(2, "Invalid timestamp type\n", 23);
			clean_exit(opt, EX_SOFTWARE);
		}
		opt->index[0] += 2;
		opt->index[1] = 0;
		if (!(node = (t_so_opt *)malloc(sizeof(t_so_opt))))
		{
			write(2, "ft_ping: failed to allocate memory"
					"on the heap.\n", 48);
			clean_exit(opt, EX_OSERR);
		}
		else if (!opt->so_opt)
			opt->so_opt = node;
		else
		{
			next = opt->so_opt;
			while (next->next)
				next = next->next;
			next->next = node;
		}
		node->next = NULL;
		node->func = &set_so_timestamp;
		return (true);
	}
	return (false);
}
