#ifndef FT_PING_H
# define FT_PING_H

# include <arpa/inet.h>
# include <ifaddrs.h>
# include <netdb.h>
# include <netinet/ip_icmp.h>
# include <signal.h>
# include <stdbool.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/capability.h>
# include <sys/time.h>
# include <sysexits.h>
# include <time.h>
# include <unistd.h>
#include <net/if.h>

/*
** ALL MACRO DEFINE AND PARSER PART
*/

# define VERBOSE 1
# define FLOOD 2
# define INTERFACE 4
# define NUMERIC 8
# define DEADLINE 16
# define TIMEOUT 32
# define INTERVAL 64
# define TSONLY 128
# define TSANDADDR 256
# define TSPRESPEC 512
# define EOO 1024

typedef struct			s_prespec{
	struct s_prespec	*next;
	struct addrinfo		*host;
}						t_prespec;

typedef struct s_opt	t_opt;

typedef bool			(*Func)(int, t_opt *);

typedef struct			s_so_opt{
	Func				func;
	struct s_so_opt		*next;
}						t_so_opt;

struct					s_opt{
	int					index[2];
	int					info;
	int					sockfd;
	struct	addrinfo	*destination;
	int					preload;
	int					ttl;
	int					quality;
	struct timeval		deadline;
	struct timeval		timeout;
	struct timeval		interval;
	t_prespec			*prespec;
	struct ifreq		ifr;				
	int					hops;
	char				itf_bin[39];
	int					ac;
	t_so_opt			*so_opt;
	timer_t				ptimer[2];
};

void					clean_exit(t_opt *opt, int exit_value);
void					display_help();
bool					f_is_digit(const char *to_eval,
						size_t *lst_dig, bool time);
int						ft_atoi(const char *s, size_t lst_dig);
bool					ft_strcmp(const char *s1, const char *s2);
struct addrinfo			*get_destination(char *dest);
bool					set_deadline(t_opt *opt, char **av);
bool					set_flood(t_opt *opt, char **av);
bool					set_help(t_opt *opt, char **av);
bool					set_interface(t_opt *opt, char **av);
bool					set_interval(t_opt *opt, char **av);
void					set_node(t_opt *opt, Func func);
bool					set_numeric(t_opt *opt, char **av);
bool					set_preload(t_opt *opt, char **av);
bool					set_quality(t_opt *opt, char **av);
bool					set_so_interface(int sockfd, t_opt *opt);
bool					set_so_quality(int sockfd, t_opt *opt);
bool					set_so_timestamp(int sockfd, t_opt *opt);
bool					set_so_ttl(int sockfd, t_opt *opt);
bool					set_timeout(t_opt *opt, char **av);
bool					set_timestamp(t_opt *opt, char **av);
bool					set_ttl(t_opt *opt, char **av);
bool					set_verbose(t_opt *opt, char **av);
bool					time_value(struct timeval *st,
						const char *to_eval, size_t lst_hex);

/*
** EXECUTION PART
*/

extern bool				glob_flag;

typedef struct			s_send{
	int					info;
	int					sockfd;
	pid_t				pid;
	int					ptrans;
	struct sockaddr		*to;
}						t_send;

typedef struct			s_print{
	int					info;
	int					ttl;
	int					*ptrans;
	int					pid;
	int					prec;
	int					hops;
	int					poot;
	int					brec;
	char				daddr[39];
	struct sockaddr		from;
	union {
		u_char			data[84];
		u_char			tsdata[124];
	};
	float				min;
	float				max;
	float				sum;
	float				sum2;
	struct timeval		init;
	struct timeval		timeout;
}						t_print;

void					final_stats(t_opt *opt,
						t_print *print);
void					ft_ping_exec(t_opt *opt);
void					ping_loop(t_opt *opt,
						t_send *send, t_print *print);
void					pinger(t_send *send);
void					print_receipt(t_print *print);
void					set_dltimer(t_opt *opt);
void					set_pingtimer(t_opt *opt,
						t_send *send);
float					time_calc(t_print *print,
						struct timeval *tv);
#endif
