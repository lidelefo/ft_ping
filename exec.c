#include "ft_ping.h"

bool			glob_flag;

static	void	set_privilege(t_opt *opt, bool set)
{
	cap_t		caps;
	cap_value_t	cap_list[2];
	cap_flag_t	flag;	
	
	flag = set ? CAP_EFFECTIVE : CAP_PERMITTED;
	caps = cap_get_proc();
	if (!caps)
	{
		write(2, "ft_ping failed on allocating"
		"capabilities struct\n", 49);
		clean_exit(opt, EX_UNAVAILABLE);
	}
	cap_list[0] = CAP_NET_RAW;
	cap_list[1] = CAP_NET_ADMIN;
	if (cap_set_flag(caps, flag, 2, cap_list, CAP_SET)
	== -1 || cap_set_proc(caps) == -1)
	{
		write(2, "ft_ping failed to set/unset network privileges\n", 47);
		cap_free(caps);
		clean_exit(opt, EX_NOPERM);
	}
	cap_free(caps);
}

static	int		init_socket(t_opt *opt)
{
	int ret;
	int uid;

	if ((uid = getuid()))
        set_privilege(opt, true);
	if ((ret = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP))
     != -1)
	{
		for (t_so_opt *node = opt->so_opt; node; node = node->next)
		{
        	if ((node->func)(ret, opt))
			{
				write(2, "ft_ping failed to set socket options\n", 36);
				close(ret);
				clean_exit(opt, EX_SOFTWARE);
			}
		}
	}	
	if (uid)
        set_privilege(opt, false);
	return (ret);
}

static void		stop_handler(int sig __attribute__((unused)),
				siginfo_t *si __attribute__((unused)),
				void *uc __attribute__((unused)))
{
	glob_flag = true;
	write(1, "\n", 1);
}

static void		prepare_exec(t_send *send, t_print *print, t_opt *opt)
{
	struct sigaction	sa;

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = stop_handler;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, NULL);
	glob_flag = false;
	print->prec = 0;
	print->poot = 0;
	send->ptrans = 0;
	print->ptrans = &send->ptrans;
	send->pid = getpid();
	print->pid = send->pid;
	print->min = 99;
	print->max = 0;
	print->sum = 0;
	send->info = opt->info;
	print->info = opt->info;
	print->hops = opt->hops;
	print->timeout = opt->timeout;
	send->to = opt->destination->ai_addr;
}

void			ft_ping_exec(t_opt *opt)
{
	t_send	send;
	t_print	print;

	if ((opt->sockfd = init_socket(opt)) == -1)
	{
		write(2, "ft_ping failed to create a socket", 34);
		clean_exit(opt, EX_CANTCREAT);
	}
	send.sockfd = opt->sockfd;
	prepare_exec(&send, &print, opt);
	ping_loop(opt, &send, &print);
	close(send.sockfd);
	clean_exit(opt, 0);
}
