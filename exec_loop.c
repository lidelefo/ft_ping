#include "ft_ping.h"

static unsigned short	checksum(void *addr, size_t len)
{
	int				sum;
	unsigned short	*word;

	sum = 0;
	word = addr;
	while (len > 1)
	{
		sum += *word++;
		len -=2;
	}
	if (len == 1)
		sum += *word;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	return ~sum;
}

void					pinger(t_send *send)
{
	static u_char	packet[64];
	int				sent;
	struct icmp *icmp = (struct icmp*)packet;
	struct timeval *tp = (struct timeval *)&packet[8];
	u_char *datap = &packet[8+sizeof(struct timeval)];

	icmp->icmp_id = send->pid;
	gettimeofday(tp, NULL);
	icmp->icmp_type = ICMP_ECHO;
	icmp->icmp_seq = htons(++send->ptrans);
	for (int i = 25; i < 64; ++i)
		*(datap)++ = i;
	*datap = '\0';
	icmp->icmp_cksum = 0;
	icmp->icmp_cksum = checksum(icmp, 64);
	if ((sent = sendto(send->sockfd, packet,
	64, 0, send->to, sizeof(struct sockaddr))) == -1 || sent != 64)
	{
		write(2, "ft_ping failed to send a packet\n", 32);
	 	glob_flag = true;
	}
}

static void				*setrecv(t_print *print, int *dlen)
{
	unsigned char *ret = NULL;
	if ((print->info & (TSPRESPEC | TSONLY | TSANDADDR)))
	{
		*dlen = 124;
		ret = (void *)&(print->tsdata);
	}
	else
	{
		*dlen = 84;
		ret = (void *)&(print->data);
	}
	return (ret);
}

void					ping_loop(t_opt *opt, t_send *send, t_print *print)
{
	int				i = 0;
	int				dlen = 0;
	unsigned char	*ibase = NULL;
	
	if ((opt->info & DEADLINE))
		set_dltimer(opt);
	inet_ntop(AF_INET, &((struct sockaddr_in *)opt->destination->ai_addr)->sin_addr, print->daddr, opt->destination->ai_addrlen);
	printf("FT_PING %s (%s) ", 
	opt->destination->ai_canonname, print->daddr);
	if (opt->info & INTERFACE)
		printf("from %s %s : ", opt->itf_bin, opt->ifr.ifr_name);
	gettimeofday(&print->init, NULL);
	while (++dlen <= opt->preload && !glob_flag)
		pinger(send);
	ibase = setrecv(print, &dlen);
	printf("56(%d) bytes of data.\n", dlen);
	set_pingtimer(opt, send);
	while (!glob_flag)
	{
		struct msghdr pck;
        struct iovec iov;
        pck.msg_name = &(print->from);
        pck.msg_namelen = sizeof(print->from);
        pck.msg_iovlen = 1;
        iov.iov_len = dlen;
        iov.iov_base = ibase;
        pck.msg_iov = &iov;
		if ((i = recvmsg(send->sockfd, &pck, 0)) >= 0)
		{
			print->brec = i;
			print_receipt(print);
		}
	}
	final_stats(opt, print);
}
