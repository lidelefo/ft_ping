#include "ft_ping.h"

static bool	is_hex_form(const char *to_eval, size_t *lst_hex)
{
	if (!to_eval)
		return (false);
	while (to_eval[*lst_hex] == ' ')
		++(*lst_hex);
	if (to_eval[*lst_hex] == '0' && (to_eval[*lst_hex + 1] == 'x'
	|| to_eval[*lst_hex + 1] == 'X'))
		*lst_hex += 2;
	else
		return (false);
	while ((to_eval[*lst_hex] >= '0' && to_eval[*lst_hex] <= '9')
	|| (to_eval[*lst_hex] >= 'a' && to_eval[*lst_hex] <= 'f')
	|| (to_eval[*lst_hex] >= 'A' && to_eval[*lst_hex] <= 'F'))
		++(*lst_hex);
	return (!to_eval[(*lst_hex)--]);
}

static int	hexatoi(const char *s, size_t lst_hex)
{
	int		ret;
	int		mul;
	char	ucase;

	ret = 0;
	mul = 0;
	if (!s)
		return (0);
	while (lst_hex >= 0
	&& ((s[lst_hex] <= '9' && s[lst_hex] >= '0' && !(ucase = 0))
	|| (s[lst_hex] <= 'f' && s[lst_hex] >= 'a' && (ucase = 1))
	|| (s[lst_hex] <= 'F' && s[lst_hex] >= 'A' && (ucase = 2))))
	{
		if (!ucase)
			ret += (s[lst_hex] - '0') << mul;
		else if (ucase == 1)
			ret += (s[lst_hex] - 'a' + 10) << mul;
		else
			ret += (s[lst_hex] - 'A' + 10) << mul;
		--lst_hex;
		mul += 4;
	}
	return (ret);
}

static void	check_quality(t_opt *opt)
{
	if (opt->quality <= 255 && opt->quality >= 0)
	{
		opt->index[0] += 2;
        opt->index[1] = 0;
        set_node(opt, &set_so_quality);	
	}
	else
	{
		write(2, "ft_ping: the decimal value of TOS bits must be 0..255\n", 54);
		clean_exit(opt, EX_SOFTWARE);
	}
}

bool		set_quality(t_opt *opt, char **av)
{
	size_t		lst_hex;

	lst_hex = 0;
	if (ft_strcmp(av[opt->index[0]], "-Q"))
	{
		if (is_hex_form(av[opt->index[0] + 1], &lst_hex))
			opt->quality = hexatoi(av[opt->index[0] + 1], lst_hex);
		else if (!(lst_hex = 0) && f_is_digit(av[opt->index[0] + 1], &lst_hex, false))
			opt->quality = ft_atoi(av[opt->index[0] + 1], lst_hex);
		check_quality(opt);
		return (true);
	}
	return (false);
}

bool		set_ttl(t_opt *opt, char **av)
{
	size_t		lst_dig;

	lst_dig = 0;
	if (ft_strcmp(av[opt->index[0]], "-t"))
	{
		if ((!f_is_digit(av[opt->index[0] + 1], &lst_dig, false)
		&& write(2, "ft_ping: can't set unicast time-to-live:"
		" Invalid argument\n", 58)) || (((opt->ttl = ft_atoi(av[opt->index[0] + 1], lst_dig)) > 255
		|| opt->ttl < 1) && write(2, "ft_ping: ttl out of range\n", 26)))
			clean_exit(opt, EX_SOFTWARE);
		opt->index[0] += 2;
		opt->index[1] = 0;
		set_node(opt, &set_so_ttl);
		return (true);
	}
	return (false);
}
