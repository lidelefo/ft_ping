#include "ft_ping.h"

static void	print_timestamps(t_print *print, struct ip_timestamp *ipt)
{
	struct in_addr *sin = (struct in_addr *)ipt->data;
	
	if ((print->info & TSANDADDR))
	{
		for (int i = 0;i < 8; sin = (struct in_addr *)&ipt->data[++i])
			printf("       %-15s   %u\n", inet_ntoa(*sin), ipt->data[++i]);
	}
	else if ((print->info & TSONLY))
	{
		for (int i = 0;i < 4; ++i)
			printf("              %u\n", ipt->data[i]);
	}
	else
	{
		int lim = print->hops * 2;
		for (int i = 0;i < lim; sin = (struct in_addr *)&ipt->data[++i])
			printf("       %-15s   %u \n", inet_ntoa(*sin), ipt->data[++i]);
	}
}

static void	error_detail(struct icmp *icmp)
{
	uint8_t type = icmp->icmp_type;
	uint8_t code = icmp->icmp_code;

	printf("packet header has unexpected type ");
	switch (type)
	{
		case ICMP_DEST_UNREACH:
			printf("ICMP_DEST_UNREACH (%d)", type);
			switch (code)
			{
				case ICMP_NET_UNREACH:
					printf(" with code ICMP_NET_UNREACH (%d)\n", code);
					break;
				case ICMP_HOST_UNREACH:
					printf(" with code ICMP_HOST_UNREACH (%d)\n", code);
					break;
				case ICMP_PROT_UNREACH:
					printf(" with code ICMP_PROT_UNREACH (%d)\n", code);
					break;
				case ICMP_PORT_UNREACH:
					printf(" with code ICMP_PORT_UNREACH (%d)\n", code);
					break;
				case ICMP_FRAG_NEEDED:
					printf(" with code ICMP_FRAG_NEEDED (%d)\n", code);
					break;
				case ICMP_SR_FAILED:
					printf(" with code ICMP_SR_FAILED (%d)\n", code);
					break;
				case ICMP_NET_UNKNOWN:
					printf(" with code ICMP_NET_UNKNOWN (%d)\n", code);
					break;
				case ICMP_HOST_UNKNOWN:
					printf(" with code ICMP_HOST_UNKNOWN (%d)\n", code);
					break;
				case ICMP_HOST_ISOLATED:
					printf(" with code ICMP_HOST_ISOLATED (%d)\n", code);
					break;
				case ICMP_NET_ANO:
					printf(" with code ICMP_NET_ANO (%d)\n", code);
					break;
				case ICMP_HOST_ANO:
					printf(" with code ICMP_HOST_ANO (%d)\n", code);
					break;
				case ICMP_NET_UNR_TOS:
					printf(" with code ICMP_NET_UNR_TOS (%d)\n", code);
					break;
				case ICMP_HOST_UNR_TOS:
					printf(" with code ICMP_HOST_UNR_TOS (%d)\n", code);
					break;
				case ICMP_PKT_FILTERED:
					printf(" with code ICMP_PKT_FILTERED (%d)\n", code);
					break;
				case ICMP_PREC_VIOLATION:
					printf(" with code ICMP_PREC_VIOLATION (%d)\n", code);
					break;
				case ICMP_PREC_CUTOFF:
					printf(" with code ICMP_PREC_CUTOFF (%d)\n", code);
					break;
				default:
					printf(" with code NR_ICMP_UNREACH  (%d)\n", code);
					break;
			}
			break;
		case ICMP_SOURCE_QUENCH:
			printf("ICMP_SOURCE_QUENCH (%d)\n", type);
			break;
		case ICMP_REDIRECT:
			printf("ICMP_REDIRECT (%d)", type);
			switch(code)
			{
				case ICMP_REDIR_NET:
					printf(" with code ICMP_REDIR_NET (%d)\n", code);
					break;
				case ICMP_REDIR_HOST:
					printf(" with code ICMP_REDIR_HOST (%d)\n", code);
					break;
				case ICMP_REDIR_NETTOS:
					printf(" with code ICMP_REDIR_NETTOS (%d)\n", code);
					break;
				case ICMP_REDIR_HOSTTOS:
					printf(" with code ICMP_REDIR_HOSTTOS (%d)\n", code);
					break;
			}
			break;
		case ICMP_ECHO:
			printf("ICMP_ECHO (%d)\n", type);
			break;
		case ICMP_ROUTERADVERT:
			printf("ICMP_ROUTERADVERT (%d)\n", type);
			break;
		case ICMP_ROUTERSOLICIT:
			printf("ICMP_ROUTERSOLICIT (%d)\n", type);
			break;
		case ICMP_TIME_EXCEEDED:
			printf("ICMP_TIME_EXCEEDED (%d)", type);
			switch (code)
			{
				case ICMP_EXC_TTL:
					printf(" with code ICMP_EXC_TTL (%d)\n", code);
					break;
				case ICMP_EXC_FRAGTIME:
					printf(" with code ICMP_EXC_FRAGTIME (%d)\n", code);
					break;
			}
			break;
		case ICMP_PARAMETERPROB:
			printf("ICMP_PARAMETERPROB (%d)\n", type);
			break;
		case ICMP_TIMESTAMP:
			printf("ICMP_TIMESTAMP (%d)\n", type);
			break;
		case ICMP_INFO_REQUEST:
			printf("ICMP_INFO_REQUEST (%d)\n", type);
			break;
		case ICMP_INFO_REPLY:
			printf("ICMP_INFO_REPLY (%d)\n", type);
			break;
		case ICMP_ADDRESS:
			printf("ICMP_ADDRESS (%d)\n", type);
			break;
		case ICMP_ADDRESSREPLY:
			printf("ICMP_ADDRESSREPLY (%d)\n", type);
			break;
		default:
			printf("NR_ICMP_TYPES (%d)\n", type);
			break;
	}
}

void		print_receipt(t_print *print)
{
	struct ip			*ip;
	struct icmp			*icmp;
	struct ip_timestamp	*ipt;
	struct timeval		*tv;
	char				raddr[39];
	int					hlen;
	float				tt;

	ip = (struct ip *)print->data;
	hlen = ip->ip_hl << 2;
	print->brec -= hlen;
	icmp = (struct icmp *)(print->data + hlen);
	if ((icmp->icmp_type != ICMP_ECHOREPLY && icmp->icmp_type != ICMP_TIMESTAMPREPLY) || icmp->icmp_id != print->pid)
	{
		if (print->info & VERBOSE)
			error_detail(icmp);
		return ;
	}
	tv = (struct timeval *)icmp->icmp_data;
	tt = time_calc(print, tv);
	++print->prec;
	if ((print->info & TIMEOUT) && ((float)(print->timeout.tv_sec * 1000 + print->timeout.tv_usec / 1000) / 1000) <= tt)
	{
		++print->poot;
		return;
	}
	print->ttl = ip->ip_ttl;
	if (!(print->info & FLOOD))
	{
		if (!(print->info & NUMERIC) && !getnameinfo(&print->from, sizeof(struct sockaddr), raddr, 39 , NULL, 0, NI_NAMEREQD))
			printf("%d bytes from %s (%s): ", print->brec, raddr, print->daddr);
		else
			printf("%d bytes from %s: ", print->brec, print->daddr);
		printf("icmp_seq=%d ttl=%d time=%.3f ms\n",
		print->prec, print->ttl, tt);
		if (print->info & (TSONLY | TSANDADDR | TSPRESPEC))
		{
			ipt = (struct ip_timestamp *)(ip + 1);
			print_timestamps(print, ipt);
		}
	}
	else
		write(1, ".", 1);
}

void		final_stats(t_opt *opt, t_print *print)
{
	int				perc;
	struct timeval	now;
	long			ts;

	if (!*print->ptrans)
		return ;
	perc = 100 - print->prec * 100 / *print->ptrans;
	gettimeofday(&now, NULL);
	ts = (now.tv_sec - print->init.tv_sec) * 1000
            + (now.tv_usec- print->init.tv_usec) / 1000;
	printf("--- %s ft_ping statistics ---\n%d packets transmitted,"
	" %d received, %d%% packet loss, ",
	opt->destination->ai_canonname, *print->ptrans,
	print->prec, perc);
	if (print->poot)
		printf("%d packets out of wait time, ", print->poot);
	printf("time %ldms\n", ts);
	if (print->prec)
		printf("rtt min/avg/max = %.3f/%.3f/%.3f\n", print->min, print->sum / print->prec, print->max);
}
