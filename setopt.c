#include "ft_ping.h"

static bool	is_valid_opt(char **av, t_opt *opt, char to_match)
{
	if (!av[opt->index[0]][opt->index[1]])
	{
		opt->index[1] = 0;
		++(opt->index[0]);
	}
	if (!opt->index[1] && av[opt->index[0]][opt->index[1]] != '-')
	{
		opt->info |= EOO;
		return (false);
	}
	else if (!opt->index[1])
		++opt->index[1];
	return (to_match == av[opt->index[0]][opt->index[1]]);
}

bool		set_flood(t_opt *opt, char **av)
{
	if (is_valid_opt(av, opt, 'f'))
	{
		opt->info |= FLOOD;
		++(opt->index[1]);
		if (!(opt->info & INTERVAL))
		{
			opt->interval.tv_sec = 0;
			opt->interval.tv_usec = 1000;
		}
		return (true);
	}
	return (false);
}

bool		set_help(t_opt *opt, char **av)
{
	if (is_valid_opt(av, opt, 'h'))
	{
		display_help();
		clean_exit(opt, EX__BASE);
	}
	return (false);
}

bool		set_numeric(t_opt *opt, char **av)
{
	if (is_valid_opt(av, opt, 'n'))
	{
		opt->info |= NUMERIC;
		++(opt->index[1]);
		return (true);
	}
	return (false);
}

bool		set_verbose(t_opt *opt, char **av)
{
	if (is_valid_opt(av, opt, 'v'))
	{
		opt->info |= VERBOSE;
		++(opt->index[1]);
		return (true);
	}
	return (false);
}

bool    set_preload(t_opt *opt, char **av)
{
    size_t lst_dig;

    lst_dig = 0;
    if (ft_strcmp(av[opt->index[0]], "-l"))
    {
        if (!f_is_digit(av[opt->index[0] + 1], &lst_dig, false)
        || (opt->preload = ft_atoi(av[opt->index[0] + 1], lst_dig)) < 1)
        {
            write(2, "ft_ping: bad preload value, should be"
            " 1..2147483647\n", 52);
            clean_exit(opt, EX_SOFTWARE);
        }
        opt->index[0] += 2;
        opt->index[1] = 0;
        return (true);
    }
    return (false);
}
