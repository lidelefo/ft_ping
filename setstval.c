#include "ft_ping.h"

bool	set_interval(t_opt *opt, char **av)
{
	size_t lst_dig;

	lst_dig = 0;
	if (ft_strcmp(av[opt->index[0]], "-i"))
	{
		if (!f_is_digit(av[opt->index[0] + 1], &lst_dig, true)
		|| !time_value(&opt->interval, av[opt->index[0] + 1], lst_dig) || (opt->interval.tv_sec = ft_atoi(av[opt->index[0] + 1], lst_dig)) < 0)
		{
			write(2, "ft_ping: bad timing interval\n", 29);
			clean_exit(opt, EX_SOFTWARE);
		}
		opt->info |= INTERVAL;
		opt->index[0] += 2;
		opt->index[1] = 0;
		return (true);
	}
	return (false);
}

bool        set_timeout(t_opt *opt, char **av)
{
    size_t lst_dig;

    lst_dig = 0;
    if (ft_strcmp(av[opt->index[0]], "-W"))
    {
        opt->info |= TIMEOUT;
        if (!f_is_digit(av[opt->index[0] + 1], &lst_dig, true)
        || !time_value(&opt->timeout, av[opt->index[0] + 1], lst_dig) || (opt->timeout.tv_sec = ft_atoi(av[opt->index[0] + 1], lst_dig)) < 0)
        {
            write(2, "ft_ping: bad linger time.\n", 26);
            clean_exit(opt, EX_SOFTWARE);
        }
        opt->index[0] += 2;
        opt->index[1] = 0;
        return (true);
    }
    return (false);
}

bool    set_deadline(t_opt *opt, char **av)
{
    size_t lst_dig;

    lst_dig = 0;
    if (ft_strcmp(av[opt->index[0]], "-w"))
    {
        opt->info |= DEADLINE;
        if (!f_is_digit(av[opt->index[0] + 1], &lst_dig, true)
        || !time_value(&opt->deadline, av[opt->index[0] + 1], lst_dig)
        || (opt->deadline.tv_sec = ft_atoi(av[opt->index[0] + 1], lst_dig)) < 0)
        {
            write(2, "ft_ping: bad wait time.\n", 24);
            clean_exit(opt, EX_SOFTWARE);
        }
        opt->index[0] += 2;
        opt->index[1] = 0;
        return (true);
    }
    return (false);
}
